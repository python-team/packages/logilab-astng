#!/usr/bin/make -f

# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1

PYTHON2 := $(shell pyversions -s)
PYTHON3 := $(shell py3versions -s)
LIB2 := $(shell python -c "from distutils.command.build import build ; from distutils.core import Distribution ; b = build(Distribution()) ; b.finalize_options() ; print b.build_platlib")
LOGILAB_COMMON_DIR := $(shell dirname `python -c "import logilab.common as lc ; print lc.__file__" 2>/dev/null`)

%:
	dh $@ --with python2,python3

override_dh_auto_build:
	set -e; \
	for python in $(PYTHON2) $(PYTHON3) ; do \
		NO_SETUPTOOLS=1 $$python setup.py build ; \
	done

override_dh_auto_test:
ifeq (,$(findstring nocheck,$(DEB_BUILD_OPTIONS)))
	#set -e ;\
	for python in $(PYTHON2) ; do \
		LIB2=$$($$python -c "from distutils.command.build import build ; from distutils.core import Distribution ; b = build(Distribution()) ; b.finalize_options() ; print b.build_platlib") ;\
		LOGILAB_COMMON_DIR=$$(dirname `$$python -c "import logilab.common as lc ; print lc.__file__" 2>/dev/null`) ;\
		# we need this hack because we have to initialize the namespace ;\
		touch $(CURDIR)/$(LIB2)/logilab/__init__.py ;\
		# even bigger hack: we symlink the common module in our temp build/ ;\
		# because it has to be in the same namespace as astng ;\
		ln -s $(LOGILAB_COMMON_DIR) $(CURDIR)/$(LIB2)/logilab/ ;\
		# we can now run the test, at last... ;\
		PYTHONPATH=$(CURDIR)/$(LIB2) $$python `which pytest` ;\
		# and cleanup the mess ;\
		rm -rf $(CURDIR)/$(LIB2)/logilab/__init__.py $(CURDIR)/$(LIB2)/logilab/common ;\
	done
	for python in $(PYTHON3) ; do \
		LIB3=$$($$python -c "from distutils.command.build import build ; from distutils.core import Distribution ; b = build(Distribution()) ; b.finalize_options() ; print (b.build_purelib)") ;\
		LOGILAB_COMMON_DIR3=$$(dirname `$$python -c "import logilab.common as lc ; print (lc.__file__)" 2>/dev/null`) ;\
		# we need this hack because we have to initialize the namespace ;\
		touch $(CURDIR)/$$LIB3/logilab/__init__.py ;\
		# even bigger hack: we symlink the common module in our temp build/ ;\
		# because it has to be in the same namespace as astng ;\
		ln -s $$LOGILAB_COMMON_DIR3 $(CURDIR)/$$LIB3/logilab/ ;\
		# we can now run the test, at last... ;\
		PYTHONPATH=$(CURDIR)/$$LIB3 $$python `which pytest` ;\
		# and cleanup the mess ;\
		rm -rf $(CURDIR)/$$LIB3/logilab/__init__.py $(CURDIR)/$$LIB3/logilab/common ;\
	done
endif

override_dh_auto_clean:
	NO_SETUPTOOLS=1 python setup.py clean
	NO_SETUPTOOLS=1 python3 setup.py clean

	find . -name "*.pyc" -delete
	
	rm -rf build
	dh_clean

override_dh_auto_install:
	NO_SETUPTOOLS=1 python setup.py install --no-compile \
		--root=$(CURDIR)/debian/python-logilab-astng/ \
		--install-layout=deb
	NO_SETUPTOOLS=1 python3 setup.py install --no-compile \
		--root=$(CURDIR)/debian/python3-logilab-astng/ \
		--install-layout=deb

	# remove sub-package __init__ file (created in postinst)
	rm debian/python-logilab-astng/usr/lib/python*/*-packages/logilab/__init__.py
	rm debian/python3-logilab-astng/usr/lib/python*/*-packages/logilab/__init__.py
	# remove test directory (installed in a separated package)
	rm -rf debian/python-logilab-astng/usr/lib/python*/*-packages/logilab/astng/test
	rm -rf debian/python3-logilab-astng/usr/lib/python*/*-packages/logilab/astng/test

	# install tests
	(cd test && find . -type f -not \( -path '*/CVS/*' -or -name '*.pyc' -or -name '*.pyo' -or -name '*.egg' \) -exec install -D --mode=644 {} ../debian/python-logilab-astng/usr/share/doc/python-logilab-astng/test/{} \;)
	(cd test && find . -type f -not \( -path '*/CVS/*' -or -name '*.pyc' -or -name '*.pyo' -or -name '*.egg' \) -exec install -D --mode=644 {} ../debian/python3-logilab-astng/usr/share/doc/python3-logilab-astng/test/{} \;)

	# clean up __pycache__ dirs, generated when running tests on the build dir
	find $(CURDIR)/debian/python3-logilab-astng -type d -name "__pycache__" | xargs rm -rf

override_dh_installchangelogs:
	dh_installchangelogs -i ChangeLog

override_dh_installdocs:
	dh_installdocs -i README

override_dh_compress:
	dh_compress -i -X.py -X.ini -X.xml -Xtest
